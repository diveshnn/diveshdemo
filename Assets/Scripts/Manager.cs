﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Manager : MonoBehaviour {
    public GameObject moveMan;
    public float speedSmoothTime = 0.1f;
    public GameObject blinkMan;
    Animator blink_animator, move_animator;
    public Slider moveSpeedSLider;
    public Button switchButton;
    Text switchButtonText;
    public int blink_FOV;
    public int move_FOV;
    public float blink_y,move_y;
    public GameObject blinkUI, moveUI;

    Camera cam;
    int currentState;
	// Use this for initialization
	void Start () {
        cam = Camera.main;
        switchButtonText = switchButton.transform.GetChild(0).GetComponent<Text>();
        blink_animator = blinkMan.GetComponent<Animator>();
        move_animator = moveMan.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if(currentState == 0)
        {
            
            move_animator.SetFloat("speedPercent", moveSpeedSLider.value, speedSmoothTime, Time.deltaTime);

        }
    }

    public void changeState() {
        currentState = (currentState + 1) % 2;

        if (currentState == 0)
        {
            cam.GetComponent<Camera>().fieldOfView = move_FOV;
            cam.transform.position = new Vector3(0f,move_y,2.05f);
            switchButtonText.text = "Switch to blinking";
            blinkMan.SetActive(false);
            blinkUI.SetActive(false);
            moveMan.SetActive(true);
            moveUI.SetActive(true);
        }
        else
        {
            cam.GetComponent<Camera>().fieldOfView = blink_FOV;
            cam.transform.position = new Vector3(0f, blink_y, 2.05f);
            switchButtonText.text = "Switch to movement";
            blinkMan.SetActive(true);
            blinkUI.SetActive(true);
            moveMan.SetActive(false);
            moveUI.SetActive(false);
        }
    }

    public void Blink()
    {
        blink_animator.SetTrigger("Blink");
    }

}
