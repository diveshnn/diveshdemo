﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public float walkSpeed = 2, runSpeed = 6;
    Animator animator;
    public float turnSmoothTime = 0.2f;
    public float speedSmoothTime = 0.1f;
    float turnSmoothVelocity;
    float speedSmoothVelocity;
    float currentSpeed;
    public bool canMove = true;
    public Transform cameraT;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {

        Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        if (!canMove)
            input = Vector2.zero;

        Vector2 inputDir = input.normalized;

        if(inputDir != Vector2.zero)
        {
            float targetRotation = Mathf.Atan2(inputDir.x, inputDir.y) * Mathf.Rad2Deg + cameraT.eulerAngles.y;
            transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y,targetRotation, ref turnSmoothVelocity, turnSmoothTime);

        }

        bool running = Input.GetKey(KeyCode.LeftShift);
        float targetSpeed = (running ? runSpeed : walkSpeed) * inputDir.magnitude;
        currentSpeed = Mathf.SmoothDamp(currentSpeed, targetSpeed, ref speedSmoothVelocity, speedSmoothTime);
        transform.Translate(transform.forward * currentSpeed * Time.deltaTime, Space.World);

        float animSpeedPercent =( (running) ? 1 : 0.5f ) * inputDir.magnitude;
        animator.SetFloat("speedPercent", animSpeedPercent,speedSmoothTime,Time.deltaTime);
	}
}
